express = require 'express'
router = express.Router()

fs = require 'fs'
pug = require 'pug'
html2png = require 'html2png'
imgur = require 'imgur'

# POST convert
router.post '/', (req, res) ->
  screenshot = html2png
    width: 640
    height: 480
    browser: 'phantomjs'
  html = pug.renderFile './views/convert.pug',
    code: req.body.code
  screenshot.render html, (err, data) ->
    return screenshot.error err, cb if err
    fs.writeFileSync './public/img/000.png', data
    res.render 'convert.pug',
      code: req.body.code
    screenshot.close()

module.exports = router
